# Effortles

effortles is a employee self ervice portal, for marking attendance, applying for leavs, regularization and tracking adn generation of payroll.

## Installation 

- install [python](https://www.python.org/downloads/) as the program is written in python.

- installing a virtual environment
```
pip install --user virtualenv
```

- make a virtual environment
```
python3 -m venv env
```

- activating the virtual environment
###### linux
```
source env/bin/activate
```

- install [django] framework 
```
pip install django
```

- install the required libraries mentioned in requirement.txt file

- install the postgres sql.

- Run the following command to make the database
```
python manage.py makemigrations
python manage.py migrate
```

if you find no error your database is setup properly.

- Run the following command to collect the static file 
```
python manage.py collectstatic
```

- Run the following command to run the server
```
python manage.py runserver
```
